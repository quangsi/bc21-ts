let arrNumber: Array<number> = [1, 2, 3];

let arrname: Array<string> = ["Alice", "Bob"];

// useState

// function useState() {
//   let state: number | string;

//   function getState() {
//     return state;
//   }

//   function setSate(x: number | string) {
//     state = x;
//   }
//   return { getState, setSate };
// }

// let userAge_useState = useState();
// userAge_useState.setSate(1);
// userAge_useState.setSate("1");
function useState<T extends number | string = number>() {
  let state: T;
  function getState() {
    return state;
  }
  function setSate(x: T) {
    state = x;
  }
  return { getState, setSate };
}
let numUseState = useState<number>();
let stringUstate = useState<string>();

// let booleeanUstate = useState<boolean>();
numUseState.setSate(1);
stringUstate.setSate("100");
// booleeanUstate.getState(true);
// function useState<T extends number | string = number>
// default type cho function ( generic function )
let defautlUstate = useState();

defautlUstate.setSate(100);
// defautlUstate.setSate("100");

// function useSateObject() {
//   let objectValue: {
//     first: number;
//     sencond: string;
//   };

//   function getObjectValue() {
//     return objectValue;
//   }

//   function setObjectvalue(x: number, y: string) {
//     objectValue = {
//       first: x,
//       sencond: y,
//     };
//   }

//   return { getObjectValue, setObjectvalue };
// }
function useSateObject<F, S>() {
  let objectValue: {
    first: F;
    sencond: S;
  };

  function getObjectValue<F, S>() {
    return objectValue;
  }

  function setObjectvalue(x: F, y: S) {
    objectValue = {
      first: x,
      sencond: y,
    };
  }

  return { getObjectValue, setObjectvalue };
}

let object_useState = useSateObject<number, string>();

object_useState.setObjectvalue(1, "2");
// object_useState.setObjectvalue(1, 2);
