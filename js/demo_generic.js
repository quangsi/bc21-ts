let arrNumber = [1, 2, 3];
let arrname = ["Alice", "Bob"];
// useState
// function useState() {
//   let state: number | string;
//   function getState() {
//     return state;
//   }
//   function setSate(x: number | string) {
//     state = x;
//   }
//   return { getState, setSate };
// }
// let userAge_useState = useState();
// userAge_useState.setSate(1);
// userAge_useState.setSate("1");
function useState() {
    let state;
    function getState() {
        return state;
    }
    function setSate(x) {
        state = x;
    }
    return { getState, setSate };
}
let numUseState = useState();
let stringUstate = useState();
// let booleeanUstate = useState<boolean>();
numUseState.setSate(1);
stringUstate.setSate("100");
// booleeanUstate.getState(true);
// function useState<T extends number | string = number>
// default type cho function ( generic function )
let defautlUstate = useState();
defautlUstate.setSate(100);
// defautlUstate.setSate("100");
function useSateObject() {
    let objectValue;
    function getObjectValue() {
        return objectValue;
    }
    function setObjectvalue(x, y) {
        objectValue = {
            first: x,
            sencond: y,
        };
    }
    return { getObjectValue, setObjectvalue };
}
let object_useState = useSateObject();
object_useState.setObjectvalue(1, 2);
