console.log("Hello TS");
// basic types ~ primative ~ pass by value
// variable : type
let username = "Alice";
// username = true; err
let userAge = 2;
// let userAge: number = "2"; err
let isHoliday = true;
isHoliday = false;
// isHoliday="yes" err
let isMarried = null;
let is_married = undefined;
//   desc?: string;  option property
let todo1 = {
    id: 1,
    name: "Lau nhà",
    desc: "Tốt",
};
let todo2 = {
    id: 2,
    name: "Bắt cơm",
};
let todo3 = {
    id: 3,
    name: "học bài",
    isComplete: false,
};
let alice = {
    id: 1,
    name: "Alice Nguyễn",
    age: 2,
    desc: "Học giỏi",
};
// class (Prototype) : Định dạng format object hoặc tạo ra đối tượng ( instance ) từ class đó
class NhanVien {
    constructor() { }
}
let nv1 = {
    id: 222,
    name: "tom",
};
let nv2 = new NhanVien();
// array
let numberArr = [1, 2, 4];
// generic type
let idArr = [1, 2, 3, 4];
let todoList = [{ id: 1, name: "Lau nhà", desc: "tốt" }];
let ageResponse = null;
ageResponse = 2;
// enum type: Quy định các giá trị mà biến có thể lưu
var genderType;
(function (genderType) {
    genderType[genderType["female"] = 0] = "female";
    genderType[genderType["male"] = 1] = "male";
})(genderType || (genderType = {}));
let sv1 = {
    id: 1,
    name: "tom",
    gender: genderType.female,
};
//  any and unkown: chấp nhận tất cả các giá trị của bất kì dữ liệu nào. Lưu ý là không nên lạm any, unkown
let value1 = true;
value1 = "aclie";
value1 = 234;
let value3 = value1;
let value2 = false;
value2 = "bob";
value2 = 232;
// Partial : convert all key của object thành optionial propery
let introduceCar = (carValue) => {
    console.log(carValue.name);
};
introduceCar({ id: 1, name: "VinFast" });
let myProcut = {
    id: 1,
    name: "iphone 11",
    price: 200,
};
// Required:  chuyển all property thành bắt buộc
const myTodo = {
    id: 1,
    title: "Do homework",
    desc: "Tốt",
};
const employeeRecord = {
    Alice: {
        id: 1,
        age: 2,
    },
    Bob: {
        id: 2,
        age: 2,
    },
    Tom: { id: 3, age: 2 },
};
