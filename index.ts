console.log("Hello TS");

// basic types ~ primative ~ pass by value

// variable : type

let username: string = "Alice";
// username = true; err
let userAge: number = 2;
// let userAge: number = "2"; err
let isHoliday: boolean = true;
isHoliday = false;
// isHoliday="yes" err

let isMarried: null = null;

let is_married: undefined = undefined;

//  pass by reference : array, object,...

// interface : dùng để định dạng format của object, giúp ràng buộc dữ liệu của object và sẽ dc remove sau khi build sang js

interface Todo {
  id: number;
  name: string;
  desc?: string;
}

interface NewTodo extends Todo {
  isComplete: boolean;
}
//   desc?: string;  option property
let todo1: Todo = {
  id: 1,
  name: "Lau nhà",
  desc: "Tốt",
};
let todo2: Todo = {
  id: 2,
  name: "Bắt cơm",
};

let todo3: NewTodo = {
  id: 3,
  name: "học bài",
  isComplete: false,
};

// type : cũng định dạng format của object

type User = {
  id: number;
  name: string;
  age: number;
  desc: string;
};

let alice = {
  id: 1,
  name: "Alice Nguyễn",
  age: 2,
  desc: "Học giỏi",
};

// class (Prototype) : Định dạng format object hoặc tạo ra đối tượng ( instance ) từ class đó

class NhanVien {
  id: number;
  name: string;
  constructor() {}
}

let nv1: NhanVien = {
  id: 222,
  name: "tom",
};

let nv2 = new NhanVien();
// array

let numberArr: number[] = [1, 2, 4];
// generic type
let idArr: Array<number> = [1, 2, 3, 4];

let todoList: Todo[] = [{ id: 1, name: "Lau nhà", desc: "tốt" }];

// advanced type

// union type : cho phép 1 biến chứa nhiều kiểu dữ liệu khác nhau

type ResponseAgeBE = null | number;

let ageResponse = null;

ageResponse = 2;

// enum type: Quy định các giá trị mà biến có thể lưu

enum genderType {
  female = 0,
  male = 1,
}
interface SV {
  id: number;
  name: string;
  gender: genderType;
}
let sv1: SV = {
  id: 1,
  name: "tom",
  gender: genderType.female,
};

//  any and unkown: chấp nhận tất cả các giá trị của bất kì dữ liệu nào. Lưu ý là không nên lạm any, unkown

let value1: any = true;

value1 = "aclie";
value1 = 234;

let value3: number = value1;

let value2: unknown = false;

value2 = "bob";
value2 = 232;

// let value4: number = value2;

interface Car {
  id: number;
  name: string;
  price: number;
  desc: string;
}
// Partial : convert all key của object thành optionial propery
let introduceCar = (carValue: Partial<Car>) => {
  console.log(carValue.name);
};

introduceCar({ id: 1, name: "VinFast" });

interface Product {
  id: number;
  name: string;
  price: number;
}

let myProcut: Readonly<Product> = {
  id: 1,
  name: "iphone 11",
  price: 200,
};

// myProcut.id = 2; err
/***
 *
 *
 * const myProduct={
 *
 * id:1,
 * name:"Iphone 11",
 * price:200
 *
 * }
 * myProcut.id=2
 *
 *
 *
 */

interface TodoProps {
  id: number;
  title: string;
  desc?: string;
}
// Required:  chuyển all property thành bắt buộc

const myTodo: Required<TodoProps> = {
  id: 1,
  title: "Do homework",
  desc: "Tốt",
};
// Record type:  Record<Key,Type>

interface EmployeeInfo {
  id: number;
  age: number;
}

type EmployeeName = "Alice" | "Bob" | "Tom";

const employeeRecord: Record<EmployeeName, EmployeeInfo> = {
  Alice: {
    id: 1,
    age: 2,
  },
  Bob: {
    id: 2,
    age: 2,
  },
  Tom: { id: 3, age: 2 },
};
